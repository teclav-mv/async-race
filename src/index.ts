import Router from './scripts/utils/router';
import './style.scss';

window.addEventListener('hashchange', Router);
window.addEventListener('load', Router);
